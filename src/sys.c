#include "sys.h"
#include "FlashOS.h"
#include "w25qxx.h"
#include "qspi.h"

#define QSPI_TYPE W25Q256

__attribute__((section(".rodata.FlashDevice"))) struct FlashDevice const FlashDevice={
    FLASH_DRV_VERS,                         // Driver Version, do not modify!
    "STM32F767_W25Q256_AlientekApollo",        // Device Name
    EXTSPI,                                 // Device Type
    0x90000000,                             // Device Start Address
    (uint32)((1<<((QSPI_TYPE&0XFF)+1))),    // Device Size (16MB)
    4096,                                   // Programming Page Size
    0,                                      // Reserved, must be 0
    0xFF,                                   // Initial Content of Erased Memory
    1000,                                   // Program Page Timeout 300 mSec
    3000,                                   // Erase Sector Timeout 3000 mSec
    0x001000,0x000000,                      // Sector Size 4kB (4096 Sectors)
    SECTOR_END
};

//////////////////////////////////////////////////////////////////////////////////
//GPIO设置专用宏定义
#define GPIO_MODE_IN        0        //普通输入模式
#define GPIO_MODE_OUT       1        //普通输出模式
#define GPIO_MODE_AF        2        //AF功能模式
#define GPIO_MODE_AIN       3        //模拟输入模式

#define GPIO_SPEED_LOW      0        //GPIO速度(低速,12M)
#define GPIO_SPEED_MID      1        //GPIO速度(中速,60M)
#define GPIO_SPEED_FAST     2        //GPIO速度(快速,85M)
#define GPIO_SPEED_HIGH     3        //GPIO速度(高速,100M)

#define GPIO_PUPD_NONE      0        //不带上下拉
#define GPIO_PUPD_PU        1        //上拉
#define GPIO_PUPD_PD        2        //下拉
#define GPIO_PUPD_RES       3        //保留

#define GPIO_OTYPE_PP       0        //推挽输出
#define GPIO_OTYPE_OD       1        //开漏输出

//GPIO引脚位置定义
#define PIN0                1<<0
#define PIN1                1<<1
#define PIN2                1<<2
#define PIN3                1<<3
#define PIN4                1<<4
#define PIN5                1<<5
#define PIN6                1<<6
#define PIN7                1<<7
#define PIN8                1<<8
#define PIN9                1<<9
#define PIN10               1<<10
#define PIN11               1<<11
#define PIN12               1<<12
#define PIN13               1<<13
#define PIN14               1<<14
#define PIN15               1<<15

static void GPIO_AF_Set(GPIO_TypeDef*GPIOx,u8 BITx,u8 AFx)
{
    GPIOx->AFR[BITx>>3]&=~(0X0F<<((BITx&0X07)*4));
    GPIOx->AFR[BITx>>3]|=(u32)AFx<<((BITx&0X07)*4);
}

static void GPIO_Set(GPIO_TypeDef*GPIOx,u32 BITx,u32 MODE,u32 OTYPE,u32 OSPEED,u32 PUPD)
{
    u32 pinpos=0,pos=0,curpin=0;
    for(pinpos=0;pinpos<16;pinpos++)
    {
        pos=1<<pinpos;    //一个个位检查
        curpin=BITx&pos;//检查引脚是否要设置
        if(curpin==pos)    //需要设置
        {
            GPIOx->MODER&=~(3<<(pinpos*2));    //先清除原来的设置
            GPIOx->MODER|=MODE<<(pinpos*2);    //设置新的模式
            if((MODE==0X01)||(MODE==0X02))    //如果是输出模式/复用功能模式
            {
                GPIOx->OSPEEDR&=~(3<<(pinpos*2));    //清除原来的设置
                GPIOx->OSPEEDR|=(OSPEED<<(pinpos*2));//设置新的速度值
                GPIOx->OTYPER&=~(1<<pinpos);        //清除原来的设置
                GPIOx->OTYPER|=OTYPE<<pinpos;        //设置新的输出模式
            }
            GPIOx->PUPDR&=~(3<<(pinpos*2));    //先清除原来的设置
            GPIOx->PUPDR|=PUPD<<(pinpos*2);    //设置新的上下拉
        }
    }
}

static void GPIO_Pin_Set(GPIO_TypeDef*GPIOx,u16 pinx,u8 status)
{
    if(status&0X01){GPIOx->BSRR=pinx;}
    else{GPIOx->BSRR=pinx<<16;}
}

static u8 GPIO_Pin_Get(GPIO_TypeDef*GPIOx,u16 pinx)
{
    if(GPIOx->IDR&pinx){return 1;}
    else{return 0;}
}

void ledToggle()
{
    GPIO_Pin_Set(GPIOB,PIN1,!GPIO_Pin_Get(GPIOB,PIN1));
}

void gpioInitAll(void)
{
    RCC->AHB1ENR|=1<<0;
    RCC->AHB1ENR|=1<<1;
    RCC->AHB1ENR|=1<<2;
    RCC->AHB1ENR|=1<<3;
    RCC->AHB1ENR|=1<<4;
    RCC->AHB1ENR|=1<<5;
    //LED
    GPIO_Set(GPIOB,PIN1,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_HIGH,GPIO_PUPD_NONE);
    GPIO_Pin_Set(GPIOB,PIN1,0);

    GPIO_Set(GPIOB,1<<2,GPIO_MODE_AF,GPIO_OTYPE_PP,GPIO_SPEED_HIGH,GPIO_PUPD_PU);    //PB2复用功能输出
    GPIO_Set(GPIOB,1<<6,GPIO_MODE_AF,GPIO_OTYPE_PP,GPIO_SPEED_HIGH,GPIO_PUPD_PU);    //PB6复用功能输出
    GPIO_Set(GPIOF,0XF<<6,GPIO_MODE_AF,GPIO_OTYPE_PP,GPIO_SPEED_HIGH,GPIO_PUPD_PU);    //PF6~9复用功能输出
    GPIO_AF_Set(GPIOB,2,9);        //PB2,AF9
    GPIO_AF_Set(GPIOB,6,10);    //PB6,AF10
    GPIO_AF_Set(GPIOF,6,9);        //PF6,AF9
    GPIO_AF_Set(GPIOF,7,9);        //PF7,AF9
    GPIO_AF_Set(GPIOF,8,10);    //PF8,AF10
    GPIO_AF_Set(GPIOF,9,10);    //PF9,AF10
}

static u8 Sys_Clock_Set(u32 plln,u32 pllm,u32 pllp,u32 pllq)
{
    u16 retry=0;
    u8 status=0;
    RCC->CR|=1<<0;                //HSI 开启
    while(((RCC->CR&(1<<1))==0)&&(retry<0X1FFF)){retry++;}//等待HSI RDY
    if(retry==0X1FFF)
    {
        status=1;    //HSi无法就绪
    }
    else
    {
        RCC->APB1ENR|=1<<28;        //电源接口时钟使能
        PWR->CR1|=3<<14;            //高性能模式,时钟可到180Mhz
        PWR->CR1|=1<<16;            //使能过驱动,频率可到216Mhz
        PWR->CR1|=1<<17;            //使能过驱动切换
        RCC->CFGR|=(0<<4)|(5<<10)|(4<<13);//HCLK 不分频;APB1 4分频;APB2 2分频.
        RCC->CR&=~(1<<24);          //关闭主PLL
        RCC->PLLCFGR=pllm|(plln<<6)|(((pllp>>1)-1)<<16)|(pllq<<24);//配置主PLL,PLL时钟源来自HSI
        RCC->CR|=1<<24;             //打开主PLL
        while((RCC->CR&(1<<25))==0){}//等待PLL准备好
        FLASH->ACR|=1<<8;           //指令预取使能.
        FLASH->ACR|=7<<0;           //8个CPU等待周期.
        RCC->CFGR&=~(3<<0);         //清零
        RCC->CFGR|=2<<0;            //选择主PLL作为系统时钟
        while((RCC->CFGR&(3<<2))!=(2<<2)){}//等待主PLL作为系统时钟成功.
    }
    return status;
}

int Init(unsigned long adr,unsigned long clk,unsigned long fnc)
{
    extern uint32_t base_adr;
    base_adr=adr;
    RCC->CR|=0x00000001;        //设置HISON,开启内部高速RC振荡
    RCC->CFGR=0x00000000;       //CFGR清零
    RCC->CR&=0xFEF6FFFF;        //HSEON,CSSON,PLLON清零
    RCC->PLLCFGR=0x24003010;    //PLLCFGR恢复复位值
    RCC->CR&=~(1<<18);          //HSEBYP清零,外部晶振不旁路
    RCC->CIR=0x00000000;        //禁止RCC时钟中断
    Sys_Clock_Set(216,8,2,9);   //设置时钟400MHz，使用HSI 64MHz RC
    gpioInitAll();
    QSPI_Init(); //初始化QSPI
    return W25QXX_Init(QSPI_TYPE);
}

int UnInit(unsigned long fnc)
{
    RCC->CFGR=0x00000000;       //重设时钟
    return (0);
}
