#ifndef __SYS_H
#define __SYS_H

#include "stm32f7xx.h"
#include "core_cm7.h"
#include "stm32f767xx.h"
///////////////////////////////////////////////////////////////////////////////////
//定义一些常用的数据类型短关键字
typedef uint32_t u32;

typedef uint16_t u16;

typedef uint8_t u8;

typedef volatile uint32_t vu32;

typedef volatile uint16_t vu16;

typedef volatile uint8_t vu8;

typedef uint8_t uint8;      // 无符号  8 bits
typedef uint16_t uint16;    // 无符号 16 bits
typedef uint32_t uint32;    // 无符号 32 bits
typedef uint64_t uint64;    // 无符号 64 bits

typedef int8_t int8;        // 有符号  8 bits
typedef int16_t int16;      // 有符号 16 bits
typedef int32_t int32;      // 有符号 32 bits
typedef int64_t int64;      // 有符号 64 bits

//////////////////////////////////////////////////////////////////////////////////
uint8 initAll(void);
void ledToggle(void);
#endif











