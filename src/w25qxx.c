#include "w25qxx.h"
#include "qspi.h"

static volatile uint8 B4=0;

//初始化SPI FLASH
uint8 W25QXX_Init(uint16 deviceType)
{
    u8 temp;
    W25QXX_QReset();
    for(volatile u32 count=0;count<400000;count++){__nop();}
    W25QXX_Qspi_Enable();
    const uint16 readType=W25QXX_ReadID();
    if(readType!=deviceType){return 1;}
    if((readType&0XF)==0X08)
    {
        temp=W25QXX_ReadSR(3); //读取状态寄存器3，判断地址模式
        if((temp&0X01)==0) //如果不是4字节地址模式,则进入4字节地址模式
        {
            W25QXX_Write_Enable(); //写使能
            QSPI_Send_CMD(W25X_Enable4ByteAddr,0,(0<<6)|(0<<4)|(0<<2)|(3<<0),0);
        }
        B4=3;
    }
    else
    {
        B4=2;
    }
    W25QXX_Write_Enable(); //写使能
    QSPI_Send_CMD(W25X_SetReadParam,0,(3<<6)|(0<<4)|(0<<2)|(3<<0),0);
    temp=3<<4; //设置P4&P5=11,8个dummy clocks,104MHz
    QSPI_Transmit(&temp,1); //发送1个字节
    return 0;
}

//W25QXX进入QSPI模式
void W25QXX_Qspi_Enable(void)
{
    u8 stareg2;
    stareg2=W25QXX_ReadSR(2); //先读出状态寄存器2的原始值
    if((stareg2&0X02)==0) //QE位未使能
    {
        W25QXX_Write_Enable(); //写使能
        stareg2|=1<<1; //使能QE位
        W25QXX_Write_SR(2,stareg2); //写状态寄存器2
    }
    //写command指令,地址为0,无数据_8位地址_无地址_单线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_EnterQPIMode,0,(0<<6)|(0<<4)|(0<<2)|(1<<0),0);
}

//读取W25QXX的状态寄存器，W25QXX一共有3个状态寄存器
//状态寄存器1：
//BIT7  6   5   4   3   2   1   0
//SPR   RV  TB BP2 BP1 BP0 WEL BUSY
//SPR:默认0,状态寄存器保护位,配合WP使用
//TB,BP2,BP1,BP0:FLASH区域写保护设置
//WEL:写使能锁定
//BUSY:忙标记位(1,忙;0,空闲)
//默认:0x00
//状态寄存器2：
//BIT7  6   5   4   3   2   1   0
//SUS   CMP LB3 LB2 LB1 (R) QE  SRP1
//状态寄存器3：
//BIT7      6    5    4   3   2   1   0
//HOLD/RST  DRV1 DRV0 (R) (R) WPS ADP ADS
//regno:状态寄存器号，范:1~3
//返回值:状态寄存器值
u8 W25QXX_ReadSR(u8 regno)
{
    u8 byte=0,command=0;
    switch(regno)
    {
        case 1:command=W25X_ReadStatusReg1; //读状态寄存器1指令
            break;
        case 2:command=W25X_ReadStatusReg2; //读状态寄存器2指令
            break;
        case 3:command=W25X_ReadStatusReg3; //读状态寄存器3指令
            break;
        default:command=W25X_ReadStatusReg1;
            break;
    }
    //QPI,写command指令,地址为0,4线传数据_8位地址_无地址_4线传输指令,无空周期,1个字节数据
    QSPI_Send_CMD(command,0,(3<<6)|(0<<4)|(0<<2)|(3<<0),0);

    QSPI_Receive(&byte,1);
    return byte;
}

//写W25QXX状态寄存器
void W25QXX_Write_SR(u8 regno,u8 sr)
{
    u8 command=0;
    switch(regno)
    {
        case 1:command=W25X_WriteStatusReg1; //写状态寄存器1指令
            break;
        case 2:command=W25X_WriteStatusReg2; //写状态寄存器2指令
            break;
        case 3:command=W25X_WriteStatusReg3; //写状态寄存器3指令
            break;
        default:command=W25X_WriteStatusReg1;
            break;
    }
    //QPI,写command指令,地址为0,4线传数据_8位地址_无地址_4线传输指令,无空周期,1个字节数据
    QSPI_Send_CMD(command,0,(3<<6)|(0<<4)|(0<<2)|(3<<0),0);
    QSPI_Transmit(&sr,1);
}

//W25QXX写使能
//将S1寄存器的WEL置位
void W25QXX_Write_Enable(void)
{
    //QPI,写使能指令,地址为0,无数据_8位地址_无地址_4线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_WriteEnable,0,(0<<6)|(0<<4)|(0<<2)|(3<<0),0);
}

//W25QXX写禁止
//将WEL清零
void W25QXX_Write_Disable(void)
{
    //QPI,写禁止指令,地址为0,无数据_8位地址_无地址_4线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_WriteDisable,0,(0<<6)|(0<<4)|(0<<2)|(3<<0),0);
}

u16 W25QXX_ReadID(void)
{
    u8 temp[2];
    u16 deviceid;
    //QPI,读id,地址为0,4线传输数据_24位地址_4线传输地址_4线传输指令,无空周期,2个字节数据
    QSPI_Send_CMD(W25X_ManufactDeviceID,0,(3<<6)|(2<<4)|(3<<2)|(3<<0),0);
    QSPI_Receive(temp,2);
    deviceid=(temp[0]<<8)|temp[1];
    return deviceid;
}

//读取SPI FLASH,仅支持QPI模式
//在指定地址开始读取指定长度的数据
//pBuffer:数据存储区
//ReadAddr:开始读取的地址(最大32bit)
//NumByteToRead:要读取的字节数(最大65535)
void W25QXX_Read(u8*pBuffer,u32 ReadAddr,u16 NumByteToRead)
{
    //QPI,快速读数据,地址为ReadAddr,4线传输数据_24位地址_4线传输地址_4线传输指令,8空周期,NumByteToRead个数据
    QSPI_Send_CMD(W25X_FastReadData,ReadAddr,(3<<6)|(B4<<4)|(3<<2)|(3<<0),8);
    QSPI_Receive(pBuffer,NumByteToRead);
}

//SPI在一页(0~65535)内写入少于256个字节的数据
//在指定地址开始写入最大256字节的数据
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(最大32bit)
//NumByteToWrite:要写入的字节数(最大256),该数不应该超过该页的剩余字节数!!!
void W25QXX_Write_Page(u8*pBuffer,u32 WriteAddr,u16 NumByteToWrite)
{
    W25QXX_Write_Enable(); //写使能
    //QPI,页写指令,地址为WriteAddr,4线传输数据_24位地址_4线传输地址_4线传输指令,无空周期,NumByteToWrite个数据
    QSPI_Send_CMD(W25X_PageProgram,WriteAddr,(3<<6)|(B4<<4)|(3<<2)|(3<<0),0);
    QSPI_Transmit(pBuffer,NumByteToWrite);
    W25QXX_Wait_Busy(); //等待写入结束
}

//无检验写SPI FLASH
void W25QXX_Write_NoCheck(u8*pBuffer,u32 WriteAddr,u16 NumByteToWrite)
{
    u16 pageremain=256-WriteAddr%256;
    if(NumByteToWrite<=pageremain)
    {
        pageremain=NumByteToWrite;
    }
    while(1)
    {
        W25QXX_Write_Page(pBuffer,WriteAddr,pageremain);
        if(NumByteToWrite==pageremain)
        {
            break;
        }
        else
        {
            pBuffer+=pageremain;
            WriteAddr+=pageremain;
            NumByteToWrite-=pageremain;
            if(NumByteToWrite>256)
            {
                pageremain=256;
            }
            else
            {
                pageremain=NumByteToWrite;
            }
        }
    }
}

//擦除整个芯片
void W25QXX_Erase_Chip(void)
{
    W25QXX_Write_Enable(); //SET WEL
    W25QXX_Wait_Busy();
    //QPI,写全片擦除指令,地址为0,无数据_8位地址_无地址_4线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_ChipErase,0,(0<<6)|(0<<4)|(0<<2)|(3<<0),0);
    W25QXX_Wait_Busy(); //等待芯片擦除结束
}

//擦除一个扇区
void W25QXX_Erase_Sector(u32 Dst_Addr)
{
    W25QXX_Write_Enable(); //SET WEL
    W25QXX_Wait_Busy();
    //QPI,写扇区擦除指令,地址为0,无数据_24位地址_4线传输地址_4线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_SectorErase,Dst_Addr,(0<<6)|(B4<<4)|(3<<2)|(3<<0),0);
    W25QXX_Wait_Busy(); //等待擦除完成
}

//等待空闲
void W25QXX_Wait_Busy(void)
{
    while((W25QXX_ReadSR(1)&0x01)==0x01){} //等待BUSY位清空
}

//强制复位
void W25QXX_QReset(void)
{
    //QPI,复位使能指令,地址为0,无数据_8位地址_无地址_4线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_EnableReset,0,(0<<6)|(0<<4)|(0<<2)|(3<<0),0);
    //QPI,复位设备指令,地址为0,无数据_8位地址_无地址_4线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_ResetDevice,0,(0<<6)|(0<<4)|(0<<2)|(3<<0),0);
    //SPI,复位使能指令,地址为0,无数据_8位地址_无地址_单线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_EnableReset,0,(0<<6)|(0<<4)|(0<<2)|(1<<0),0);
    //SPI,复位设备指令,地址为0,无数据_8位地址_无地址_单线传输指令,无空周期,0个字节数据
    QSPI_Send_CMD(W25X_ResetDevice,0,(0<<6)|(0<<4)|(0<<2)|(1<<0),0);
}
