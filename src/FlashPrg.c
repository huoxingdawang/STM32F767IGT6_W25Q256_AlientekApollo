/* -----------------------------------------------------------------------------
 * Copyright (c) 2016 ARM Ltd.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from
 * the use of this software. Permission is granted to anyone to use this
 * software for any purpose, including commercial applications, and to alter
 * it and redistribute it freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software in
 *    a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 *
 * $Date:        10. October 2018
 * $Revision:    V1.0.0
 *
 * Project:      Flash Device Description for
 *               STM32H750 W25Q64 SPIFI Flash
 * --------------------------------------------------------------------------- */

#include "FlashOS.H"        // FlashOS Structures
#include "sys.h"
#include "qspi.h"
#include "w25qxx.h"
/*----------------------------------------------------------------------------*/
#define  PAGE_SIZE 4096

/*----------------------------------------------------------------------------*/
uint8_t read_buf[PAGE_SIZE]; //校验时缓存一个扇区的数据
uint32_t base_adr; //SPI Flash基址，通常为0x90000000
/*----------------------------------------------------------------------------*/

int EraseChip(void)
{
    W25QXX_Erase_Chip();
    return (0);
}

int EraseSector(unsigned long adr)
{
    ledToggle();
    W25QXX_Erase_Sector(adr-base_adr);
    return (0);
}

int BlankCheck(unsigned long adr,unsigned long sz,unsigned char pat)
{
    return (1);
}

int ProgramPage(unsigned long adr,unsigned long sz,unsigned char*buf)
{
    ledToggle();
    W25QXX_Write_NoCheck(buf,adr-base_adr,sz);
    return (0);
}

unsigned long Verify(unsigned long adr,unsigned long sz,unsigned char*buf)
{
    unsigned long i;
    ledToggle();
    W25QXX_Read(read_buf,adr-base_adr,sz);
    for(i=0;i<sz;i++)
    {
        if(read_buf[i]!=buf[i])
        {
            return (adr+i);
        }
    }
    return (adr+sz);
}
